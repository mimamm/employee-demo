package com.example.EmployeeImam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeImamApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeImamApplication.class, args);
	}

}
