package com.example.EmployeeImam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeImam.models.Pendapatan;

@Repository
public interface PendapatanRepository extends JpaRepository<Pendapatan, Long> {

}
