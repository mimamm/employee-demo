package com.example.EmployeeImam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeImam.models.Parameter;

@Repository
public interface ParameterRepository extends JpaRepository<Parameter, Long> {

}
