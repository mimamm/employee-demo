package com.example.EmployeeImam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeImam.models.PresentaseGaji;

@Repository
public interface PresentaseGajiRepository extends JpaRepository<PresentaseGaji, Long> {

}