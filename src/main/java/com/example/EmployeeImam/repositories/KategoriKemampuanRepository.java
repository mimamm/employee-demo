package com.example.EmployeeImam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeImam.models.KategoriKemampuan;

@Repository
public interface KategoriKemampuanRepository extends JpaRepository<KategoriKemampuan, Long> {

}
