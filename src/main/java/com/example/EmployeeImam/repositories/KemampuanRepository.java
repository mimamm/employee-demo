package com.example.EmployeeImam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeImam.models.Kemampuan;

@Repository
public interface KemampuanRepository extends JpaRepository<Kemampuan, Long> {
	
}