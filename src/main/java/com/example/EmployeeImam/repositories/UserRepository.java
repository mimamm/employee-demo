package com.example.EmployeeImam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeImam.models.User;
import com.example.EmployeeImam.models.UserId;

@Repository
public interface UserRepository extends JpaRepository<User, UserId> {

}
