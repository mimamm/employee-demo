package com.example.EmployeeImam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeImam.models.Posisi;

@Repository
public interface PosisiRepository extends JpaRepository<Posisi, Long> {

}
