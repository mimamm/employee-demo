package com.example.EmployeeImam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeImam.models.Agama;

@Repository
public interface AgamaRepository extends JpaRepository<Agama, Long> {

}